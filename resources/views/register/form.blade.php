<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Form Register</title>
    </head>
    <body>
    <h1>Form Register !</h1>
        <h2>Sign Up Form</h2>
    
        <form action="welcome" method="post">
        @csrf 
        <p>First name:</p>
        <input type="text" name="fname"><br>
        <p>Last name:</p>
        <input type="text" name="lname">
        <p>Gender:</p>
       
        <input type="radio" name="gender" value="male">
        <label>Male</label><br>
        <input type="radio" name="gender" value="female">
        <label>Female</label><br>
        <input type="radio" name="gender" value="other">
        <label>Other</label><br>
    
        <p>Nationality:</p>
        <select name="nationality">
        <option value="-">-- Please Select -- </option>
        <option value="indonesian" selected>Indonesian</option>
        <option value="singapore">Singapore</option>
        <option value="us">United States</option>
        </select> 

        <p>Language Spoken:</p>
        <input type="checkbox" name="language1" value="indonesia">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" name="language3" value="english">
        <label>English</label><br>
        <input type="checkbox" name="language3" value="other">
        <label>Other</label><br>

        <p>Bio:</p>
        <textarea name="bio" style="width:300px; height:150px;">
        </textarea> <br>

        <input type="submit" value="Sign Up">
        </form>
     </body>
</html>
